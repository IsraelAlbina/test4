FROM node:14

WORKDIR .

COPY product_test/ .

RUN npm install

CMD ["npm","start"]